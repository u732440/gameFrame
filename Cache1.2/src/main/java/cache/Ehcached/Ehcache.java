/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package cache.Ehcached;

/**
 * Created with IntelliJ IDEA.
 * User: CLINUX
 * Date: 12-11-25
 * Time: 下午8:24
 * To change this template use File | Settings | File Templates.
 *
 */
//$Id: EhCache.java 10716 2006-11-03 19:05:11Z max.andersen@jboss.com $


import cache.Ehcached.Icache.Cache;
import net.sf.ehcache.Element;

import java.util.List;


/**
 * 缓存
 * EHCache
 * Element---  A Cache Element, consisting of a key, value and attributes.
 */
public class Ehcache implements Cache {

    private net.sf.ehcache.Cache cache;
    /**
     * Creates a new Hibernate pluggable cache based on a cache name.
     * <p/>
     *
     * @param cache The underlying EhCache instance to use.
     */
    public Ehcache(net.sf.ehcache.Cache cache) {
        this.cache = cache;
    }


     public List keys() throws CacheException {
        return this.cache.getKeys();
    }

    /**
     * Gets a value of an element which matches the given key.
     *
     * @param key the key of the element to return.
     * @return The value placed into the cache with an earlier put, or null if not found or expired
     * @throws CacheException
     */
    public Object get(Object key) throws CacheException {
        try {
            if ( key == null )
                return null;
            else {
                Element element = cache.get( key );
                if ( element != null )
                    return element.getObjectValue();
            }
            return null;
        }
        catch (net.sf.ehcache.CacheException e) {
            throw new CacheException( e );
        }
    }

    /**
     * Puts an object into the cache.
     *
     * @param key   a key
     * @param value a value
     * @throws CacheException if the {@link net.sf.ehcache.CacheManager}
     *                        is shutdown or another {@link Exception} occurs.
     */
    public void update(Object key, Object value) throws CacheException {
        put( key, value );
    }

    /**
     * Element :A Cache Element, consisting of a key, value and attributes.
     * Puts an object into the cache.
     *
     * @param key   a key
     * @param value a value
     * @throws CacheException if the {@link net.sf.ehcache.CacheManager}
     *                        is shutdown or another {@link Exception} occurs.
     */
    public void put(Object key, Object value) throws CacheException {
        try {
            Element element = new Element( key, value );
            cache.put( element );
        }
        catch (IllegalArgumentException e) {
            throw new CacheException( e );
        }
        catch (IllegalStateException e) {
            throw new CacheException( e );
        }
        catch (net.sf.ehcache.CacheException e) {
            throw new CacheException( e );
        }

    }

    /**
     * Removes the element which matches the key.
     * <p/>
     * If no element matches, nothing is removed and no Exception is thrown.
     *
     * @param key the key of the element to remove
     * @throws CacheException
     */
    public void remove(Object key) throws CacheException {
        try {
            cache.remove( key );
        }
        catch (IllegalStateException e) {
            throw new CacheException( e );
        }
        catch (net.sf.ehcache.CacheException e) {
            throw new CacheException( e );
        }
    }

    /**
     * 移除所有elements，但是还存在缓存，处于不可用状态
     * Remove all elements in the cache, but leave the cache
     * in a useable state.
     *
     * @throws CacheException
     */
    public void removeAll() throws CacheException {
        try {
            cache.removeAll();
        }
        catch (IllegalStateException e) {
            throw new CacheException( e );
        }
        catch (net.sf.ehcache.CacheException e) {
            throw new CacheException( e );
        }
    }

    /**
     * Remove the cache and make it unuseable.
     * 移除缓存，标记其不可用
     * @throws CacheException
     */
    public void destroy() throws CacheException {
        try {
            cache.getCacheManager().removeCache(cache.getName());
        }
        catch (IllegalStateException e) {
            throw new CacheException( e );
        }
        catch (net.sf.ehcache.CacheException e) {
            throw new CacheException( e );
        }
    }

}