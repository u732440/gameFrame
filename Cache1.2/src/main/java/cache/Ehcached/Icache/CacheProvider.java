/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package cache.Ehcached.Icache;


import cache.Ehcached.CacheException;

/**
 * Created with IntelliJ IDEA.
 * User: CLINUX
 * Date: 12-11-25
 * Time: 下午8:26
 * To change this template use File | Settings | File Templates.
 *
 */
public interface CacheProvider {

    /**
     * Configure the cache
     *
     * @param cachename the name of the cache region
     * @param autoCreate autoCreate settings
     * @param autoCreate autoCreate settings
     * @throws cache.Ehcached.CacheException
     */
    public Cache buildCache(String cachename, boolean autoCreate) throws CacheException;

    /**
     * Callback to perform any necessary initialization of the underlying cache implementation
     * during SessionFactory construction.
     *
     */
    public void registerMemoryStore() throws CacheException;

    /**
     * Callback to perform any necessary cleanup of the underlying cache implementation
     * during SessionFactory.close().
     */
    public void shutdown();

}

