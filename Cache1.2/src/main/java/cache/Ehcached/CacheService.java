/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package cache.Ehcached;


import cache.Ehcached.Icache.Cache;
import cache.Ehcached.Icache.CacheProvider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: CHENLEI
 * Date: 12-11-25
 * Time: 下午8:29
 * To change this template use File | Settings | File Templates.
 *  缓存 助手
 *  定制缓存      spring 管理缓存助手实例
 *
 *  管理  ehcache.xml 中配置的 MemoryStore，    CacheManager默认的是单例
 *
 * CacheService--- 对应多个  MemoryStore
 *
 */
public class CacheService {

    private final static Log log = LogFactory.getLog(CacheService.class);
    public static CacheProvider provider;
    private CacheService(){}

    static {
        initCacheProvider(EhCacheProvider.class.getName());
    }

    private static void initCacheProvider(String prv_name){
        try{
            CacheService.provider = (CacheProvider)Class.forName(prv_name).newInstance();
            CacheService.provider.registerMemoryStore();
            log.info("Using CacheProvider : " +prv_name);
        }catch(Exception e){
            log.fatal("Unabled to initialize cache provider:" + prv_name + ", using ehcache default.", e);
            CacheService.provider = new EhCacheProvider();
        }
    }
    private final Cache _GetCache(String cache_name, boolean autoCreate) {
        if(provider == null){
            provider = new EhCacheProvider();
        }
        return provider.buildCache(cache_name, autoCreate);
    }

    /**
     * 获取缓存中的数据
     * @param cache_name
     * @param key
     * @return
     */
    public final  Object get(String cache_name, Serializable key){
        if(cache_name!=null && key != null)
            return _GetCache(cache_name, true).get(key);
        return null;
    }


    /**
     * 写入缓存
     * @param cache_name
     * @param key
     * @param value
     */
    public final  void put(String cache_name, Serializable key, Serializable value){
        if(cache_name!=null && key != null && value!=null){
            _GetCache(cache_name, true).put(key, value);
        }else {
            log.error(new CacheException("异常了！！！"));
        }
    }

    /**
     * 清除缓冲中的某个数据
     * @param cache_name
     * @param key
     */
    public final  void remove(String cache_name, Serializable key){
        if(cache_name!=null && key != null){
            Cache cache = _GetCache(cache_name, false);
            if(cache != null)
                cache.remove(key);
        }
    }

    /**
     * clear up cache
     * @param cache_name
     */
    public final void removeAll(String cache_name){
        Cache cache = _GetCache(cache_name, false);
        if(cache != null)
            cache.removeAll();
    }


    /**
     * 更新指定的数据
     * @param cache_name
     * @param key
     * @param value
     */
    public final void update(String cache_name, Serializable key,Serializable value){
        if(cache_name!=null && key != null){
            Cache cache = _GetCache(cache_name, false);
            if(cache != null)
                cache.update(key,value);
        }
    }

    /**
     * Clean up
     * @param cache_name
     */
    public final void destroy(String cache_name){
        Cache cache = _GetCache(cache_name, false);
            if(cache != null)
                cache.destroy();
    }

}
