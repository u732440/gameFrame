/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.exceptions;

/**
 * @author 石头哥哥 </br>
 *         Project : dcserver1.3</br>
 *         Date: 11/24/13  11</br>
 *         Time: 10:20 PM</br>
 *         Connect: 13638363871@163.com</br>
 *         packageName: com.dc.gameserver.serverCore.exceptions</br>
 *         注解：   游戏数据异常以及 数据提交到数据库异常     RuntimeException
 */
public class dataException extends gameException {

    public dataException(String message) {
        super(message);
    }

    public dataException(Throwable cause) {
        super(cause);
    }

    public dataException(String message, Throwable cause) {
        super(message, cause);
    }
}
