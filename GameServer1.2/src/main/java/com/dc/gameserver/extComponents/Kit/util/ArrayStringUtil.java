/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.Kit.util;

public class ArrayStringUtil {

	private final static String SPLIT_SYMBOL = "|"; // 分隔符
	private final static String SPLIT_REGEX_SYMBOL = "\\|"; // //分隔符正则表达式
	
	/**
	 * 字符串数组格式化成字符串
	 * @param array
	 * @return
	 */
	public static String array2String(String[] array) {
		if (array == null || array.length <= 0) return null;
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < array.length - 1; i++) {
			builder.append(array[i]);
			builder.append(SPLIT_SYMBOL);
		}
		builder.append(array[array.length - 1]);
		return builder.toString();
	}
	
	/**
	 * 字符串格式化成字符串数组
	 * @param string
	 * @return
	 */
	public static String[] string2Array(String string) {
		if (string != null && !string.isEmpty()) {
			// 去掉前面的分隔符
			if (string.startsWith(SPLIT_SYMBOL)) {
				string = string.substring(1);
			}
			// 去掉后面的分隔符
			if (string.endsWith(SPLIT_SYMBOL)) {
				string = string.substring(0, string.length() - 1);
			}
		}
		if (string == null || string.isEmpty()) return null;
		
		return string.split(SPLIT_REGEX_SYMBOL);
	}
	
	public static void main(String[] args) {
		System.out.println(array2String(new String[]{"haha", "hehe"}));
		System.out.println(string2Array("kaka").length);
	}
}
