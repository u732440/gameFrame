/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.Kit.page;

import java.io.Serializable;
import java.util.List;

/**
 * 代表可以分页显示的数据集合的接口
 *
 * @param <T> 需要分页显示的数据的类型
 */
public interface Pageable<T> extends Serializable {
	/**
	 * 当前页是否有一下页
	 */
	public boolean isHasNext();

	/**
	 * 当前页是否有上一页
	 */
	public boolean isHasPrevious();

	/**
	 * 获取该查询结果集的总页数
	 */
	public int getPageCount();

	/**
	 * 获得当前页的数据
	 */
	public List<T> getElements();

	/**
	 * 查询结果的总数据个数
	 */
	public long getRecordCount();

	/**
	 * 从1开始的当前页第一个元素的索引
	 */
	public long getPageFirstElementIndex();

	/**
	 * 从1开始的当前页最后一个元素的索引
	 */
	public long getPageLastElementIndex();

	/**
	 * 一页的数据个数
	 */
	public int getPageSize();

	/**
	 * 从1开始的当前页的页数索引
	 */
	public int getPageIndex();
}
