package com.dc.gameserver.extComponents.Kit.propEncrypt;//package com.dc.gameserver.extComponents.utilsKit.propEncrypt;
//
//import org.apache.commons.codec.binary.Base64;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import javax.crypto.BadPaddingException;
//import javax.crypto.Cipher;
//import javax.crypto.IllegalBlockSizeException;
//import javax.crypto.NoSuchPaddingException;
//import javax.crypto.spec.IvParameterSpec;
//import javax.crypto.spec.SecretKeySpec;
//import java.io.UnsupportedEncodingException;
//import java.security.InvalidAlgorithmParameterException;
//import java.security.InvalidKeyException;
//import java.security.NoSuchAlgorithmException;
//
//public class AesDecryptor {
//
//    private Logger logger = LoggerFactory.getLogger(this.getClass());
//    /**
//     *
//     */
//    private String charset;
//
//    public AesDecryptor() {
//        super();
//        charset = "UTF-8";
//    }
//
//    /**
//     * @param key
//     * @param iv
//     * @param data
//     * @return
//     */
//    public byte[] decrypt(byte[] key, byte[] iv, byte[] data) {
//
//        Cipher cipher = null;
//        try {
//            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//        } catch (NoSuchAlgorithmException e) {
//            logger.error(e.getMessage(), e);
//        } catch (NoSuchPaddingException e) {
//            logger.error(e.getMessage(), e);
//        }
//
//        SecretKeySpec sKeySpec = new SecretKeySpec(key, "AES");
//        IvParameterSpec ivParamSpec = new IvParameterSpec(iv);
//
//        try {
//            cipher.init(Cipher.DECRYPT_MODE, sKeySpec, ivParamSpec);
//        } catch (InvalidKeyException e) {
//            logger.error(e.getMessage(), e);
//        } catch (InvalidAlgorithmParameterException e) {
//            logger.error(e.getMessage(), e);
//        }
//
//        byte[] decryptedData = null;
//        try {
//            decryptedData = cipher.doFinal(data);
//        } catch (IllegalBlockSizeException e) {
//            logger.error(e.getMessage(), e);
//        } catch (BadPaddingException e) {
//            logger.error(e.getMessage(), e);
//        }
//
//        return decryptedData;
//    }
//
//    /**
//     * @param key
//     * @param data
//     * @return
//     */
//    public String decryptBase642String(String key, String data) {
//        byte[] dataBytes = (new Base64(-1, null, true)).decode(data);
//        return decryptBytes2String(key, dataBytes);
//    }
//
//    /**
//     * @param key
//     * @param data
//     * @return
//     */
//    public byte[] decryptBytes2Bytes(String key, byte[] data) {
//        KeyTransformer keyTransformer = new KeyTransformer();
//        return decrypt(keyTransformer.getMD5Digest(key),
//                keyTransformer.getSHA1Digest128Bit(key), data);
//    }
//
//
//    /**
//     * @param key
//     * @param data
//     * @return
//     */
//    public String decryptBytes2String(String key, byte[] data) {
//        String dataStr = null;
//        try {
//            dataStr = new String(decryptBytes2Bytes(key, data), charset);
//        } catch (UnsupportedEncodingException e) {
//            logger.error(e.getMessage(), e);
//        }
//        return dataStr;
//    }
//
//    public String getCharset() {
//        return charset;
//    }
//
//    public void setCharset(String charset) {
//        this.charset = charset;
//    }
//}
