/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package testJson;

/**
 * Created with IntelliJ IDEA.
 * User: CHENLEI
 * Date: 12-11-17
 * Time: 下午12:37
 * To change this template use File | Settings | File Templates.
 */
public class User {
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
