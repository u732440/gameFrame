/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package RabbitmpTest;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
/**
 * @author 石头哥哥 </br>
 *         gameFrame </br>
 *         Date:14-3-12 </br>
 *         Time:下午4:13 </br>
 *         Package:{@link RabbitmpTest}</br>
 *         Comment：
 */


public abstract class EndPoint{

    protected Channel channel;
    protected Connection connection;
    protected String endPointName;

    public EndPoint(String endpointName) throws IOException{

        this.endPointName = endpointName;

        //Create a connection factory
        ConnectionFactory factory = new ConnectionFactory();

        //hostname of your rabbitmq server
        factory.setHost("localhost");
//        factory.setHost("115.28.134.193");
//        factory.setUsername("root");
//        factory.setPassword("c23f0377");

        //getting a connection
        connection = factory.newConnection();

        //creating a channel
        channel = connection.createChannel();

        //declaring a queue for this channel. If queue does not exist,
        //it will be created on the server.
        channel.queueDeclare(endpointName, false, false, false, null);
    }


    /**
     * 关闭channel和connection。并非必须，因为隐含是自动调用的。
     * @throws java.io.IOException
     */
    public void close() throws IOException{
        this.channel.close();
        this.connection.close();
    }
}